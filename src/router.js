import Vue from 'vue'
import Router from 'vue-router'
import HomeView from './views/HomeView.vue'

Vue.use(Router)

export default new Router({
    routes: [{
            path: '/',
            name: 'HomeView',
            component: HomeView,
        },
        {
            path: '/view-data',
            name: 'viewData',
            // route level code-splitting
            // this generates a separate chunk (raise-request.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
                import ( /* webpackChunkName: "raise-request" */ './views/ViewData.vue')
        }
    ]
})